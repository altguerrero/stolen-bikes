import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Layout } from "../components/Layout";
import { Home } from "../views/Home";
import { Details } from "../views/Details";

function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/details/:id" element={<Details />} />
        </Routes>
      </Layout>
    </Router>
  );
}

export default App;
