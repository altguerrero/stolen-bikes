function CardItem({ children }) {
  return (
    <div className="flex flex-col sm:flex-row p p-4 mb-8 w-full gap-4 bg-white border border-gray-300 rounded-lg shadow-lg relative">
      {children}
    </div>
  );
}

export { CardItem };
