function DetailDescription({ title, description }) {
  return (
    <div className="mb-4">
      <h2 className="text-xl sm:text-2xl font-medium mb-2">{title}</h2>
      <p className="leading-normal text-base">{description}</p>
    </div>
  );
}

export { DetailDescription };
