function CardMoreInfoItem({ content, icon }) {
  if (content) {
    return (
      <span className="flex items-center gap-1 text-xs sm:text-sm">
        {icon()} {content}
      </span>
    );
  }
}

export { CardMoreInfoItem };
