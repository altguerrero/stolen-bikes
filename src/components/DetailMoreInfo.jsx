function DetailMoreInfo({ title, items }) {
  return (
    <div className="mb-4">
      <h2 className="text-xl sm:text-2xl font-medium mb-2">{title}</h2>
      <ul className="gap-8 columns-2">
        {items.map((item, index) => {
          if (!item.value) return "";
          return (
            <li
              key={`${item.title}-${index}`}
              className="flex items-center text-sm sm:text-base mb-1 sm:mb-2"
            >
              <p>
                <span className="font-medium text-base">{item.title}: </span>{" "}
                {item.value}
              </p>
            </li>
          );
        })}
      </ul>
    </div>
  );
}

export { DetailMoreInfo };
