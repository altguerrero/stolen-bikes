function CardMoreInfo({ children }) {
  return <div className="flex flex-wrap gap-y-2 gap-x-4">{children}</div>;
}

export { CardMoreInfo };
