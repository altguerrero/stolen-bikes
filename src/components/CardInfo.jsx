function CardInfo({ children }) {
  return <div className="flex-initial">{children}</div>;
}

export { CardInfo };
