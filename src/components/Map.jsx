import {
  MapContainer,
  TileLayer,
  CircleMarker,
  Popup,
  Marker,
} from "react-leaflet";

const Map = ({ coordinates }) => {
  const redOptions = {
    color: "red",
  };

  return (
    <MapContainer center={coordinates} zoom={17} scrollWheelZoom={false}>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <CircleMarker center={coordinates} pathOptions={redOptions} radius={100}>
        <Popup>Bike stolen around this area</Popup>
      </CircleMarker>
      <Marker position={coordinates} />
    </MapContainer>
  );
};
export { Map };
