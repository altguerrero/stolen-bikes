function CardTitle({ text }) {
  if (text) {
    return (
      <h2 className="text-xl sm:text-2xl mb-2 font-medium text-gray-800">
        {text}
      </h2>
    );
  }
}

export { CardTitle };
