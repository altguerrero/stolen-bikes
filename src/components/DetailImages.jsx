function DetailImages({ title, items }) {
  if (items.length >= 1) {
    return (
      <div className="mb-4">
        <h2 className="text-2xl font-medium mb-2">{title}</h2>
        <div className="gap-2 sm:gap-4 columns-2 sm:columns-3">
          {items.map((img) => (
            <figure
              key={img.id}
              className="w-full aspect-video mb-2 sm:mb-4 rounded-lg overflow-hidden"
            >
              <img
                src={img.medium}
                alt={img.name}
                className="object-cover w-full h-full"
              />
            </figure>
          ))}
        </div>
      </div>
    );
  }
}

export { DetailImages };
