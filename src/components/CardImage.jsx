function CardImage({ src, alt, defaultImg }) {
  return (
    <figure className="flex-none w-full h-32 sm:w-32 sm:h-32 rounded-lg overflow-hidden">
      {!!src ? (
        <img src={src} alt={alt} className="object-cover h-full w-full" />
      ) : (
        <span className="bg-gray-200 flex w-full h-full items-center justify-center text-7xl">
          {defaultImg()}
        </span>
      )}
    </figure>
  );
}

export { CardImage };
