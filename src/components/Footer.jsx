function Footer() {
  return (
    <footer className="bg-gray-800 text-white h-16 flex items-center justify-center font-medium text-xs sm:text-base">
      <p
        dangerouslySetInnerHTML={{
          __html: `By @altguerrero &copy; ${new Date().getFullYear()}`,
        }}
      ></p>
    </footer>
  );
}

export { Footer };
