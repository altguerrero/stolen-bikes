function TotalResults({ total }) {
  return (
    <span className="block w-fit p-1 px-3 py-1">
      <span className="font-bold text-sm">Total:</span> {total}
    </span>
  );
}

export { TotalResults };
