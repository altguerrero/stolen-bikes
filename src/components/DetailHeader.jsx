import formatDate from "../utils/formatDate";

function DetailHeader({ title, createdAt, stonlenLocation }) {
  return (
    <header className="mb-4">
      <h1 className="font-medium text-2xl sm:text-4xl sm:mb-1">{title}</h1>
      <p className="font-normal text-base sm:text-xl">
        <span className="font-medium">Stonlen</span>{" "}
        {createdAt ? formatDate(createdAt) : "undefined"}{" "}
        <span className="font-medium">at</span>{" "}
        {stonlenLocation ? stonlenLocation : "undefined"}
      </p>
    </header>
  );
}

export { DetailHeader };
