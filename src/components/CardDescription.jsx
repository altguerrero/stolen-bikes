function CardDescription({ text }) {
  if (text) {
    return (
      <p className="text-sm sm:text-base leading-normal text-gray-700 mb-2">
        {text.length >= 180 ? `${text.substring(0, 180)} ...` : text}
      </p>
    );
  }
}

export { CardDescription };
