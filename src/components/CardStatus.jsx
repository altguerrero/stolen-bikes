function CardStatus({ status, stolen }) {
  return (
    <span className="flex items-center gap-1 text-sm px-2 font-medium bg-white border border-gray-300 rounded absolute -top-3 sm:-top-2 sm:right-0 sm:mr-6">
      <i
        className={`${
          stolen
            ? "bg-red-500 shadow-red-500 "
            : "bg-green-500 shadow-green-500"
        } shadow block w-2 h-2 rounded-full`}
      ></i>
      {status}
    </span>
  );
}

export { CardStatus };
