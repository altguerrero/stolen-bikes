import { useRef } from "react";

function Search({ icon, setSearch }) {
  const searchInput = useRef(null);

  const handleSearch = () => {
    setSearch(searchInput.current.value);
  };

  return (
    <div className="flex gap-3 w-10/12 sm:w-auto">
      <input
        type="text"
        placeholder="search"
        ref={searchInput}
        className="text-base px-3 py-2 border border-gray-300 shadow-lg rounded-lg block w-full sm:w-80 text-gray-700"
      />
      <button
        onClick={handleSearch}
        className="text-base w-12 py-2 text-white bg-gray-700 flex items-center justify-center rounded-lg shadow-lg"
      >
        {icon()}
      </button>
    </div>
  );
}

export { Search };
