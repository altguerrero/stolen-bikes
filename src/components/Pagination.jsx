import ReactPaginate from "react-paginate";
import calculatePagesCount from "../utils/calculatePagesCount";

function Pagination({ setPageNumber, pageCount, pageSize }) {
  return (
    <ReactPaginate
      className="flex justify-center items-center mb-6"
      nextLabel="Next ->"
      previousLabel="<- Prev"
      nextClassName="transition-colors duration-150 hover:bg-gray-200 px-1 sm:px-3 py-1 rounded mx-2 sm:mx-4 font-medium text-xs sm:text-base"
      previousClassName="transition-colors duration-150 hover:bg-gray-200 px-1 sm:px-3 py-1 rounded mx-2 sm:mx-4 font-medium text-xs sm:text-base"
      pageClassName="transition-colors duration-150 hover:bg-gray-600 hover:text-white cursor-pointer rounded mr-1 sm:mr-2 text-center font-medium"
      pageLinkClassName="block px-2 sm:px-3 py-1 text-xs sm:text-base"
      activeClassName="bg-gray-600 text-white"
      pageCount={calculatePagesCount(pageSize, pageCount)}
      pageRangeDisplayed={3}
      marginPagesDisplayed={1}
      onPageChange={(data) => {
        setPageNumber(data.selected + 1);
      }}
    />
  );
}

export { Pagination };
