function DetailInfo({ children }) {
  return <section className="mt-4 mb-6">{children}</section>;
}

export { DetailInfo };
