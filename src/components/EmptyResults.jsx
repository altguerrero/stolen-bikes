function EmptyResults({ icon, message }) {
  return (
    <div className="py-12 sm:py-16 text-center flex flex-col sm:flex-row items-center gap-2 justify-center text-base sm:text-lg">
      {icon()} <p>{message}</p>
    </div>
  );
}

export { EmptyResults };
