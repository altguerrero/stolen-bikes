import { Header } from "./Header";
import { Footer } from "./Footer";

function Layout({ children }) {
  return (
    <div className="Layout">
      <Header />
      <main className="container mt-24">{children}</main>
      <Footer />
    </div>
  );
}

export { Layout };
