import Logo from "../assets/img/logo.png";
import { NavLink } from "react-router-dom";

function Header() {
  return (
    <header className="bg-gray-800 shadow-lg fixed top-0 w-full z-50">
      <div className="container">
        <NavLink
          to="/"
          className="h-16 w-full px-4 sm:px-0 sm:w-8/12 mx-auto flex gap-3 items-center"
        >
          <figure className="w-12">
            <img className="" src={Logo} alt="" />
          </figure>
          <div className="text-white">
            <h1 className="font-bold text-base sm:text-lg mb-0">
              Police Departament of Berlin
            </h1>
            <h2 className="font-medium text-xs sm:text-sm">Stolen Bykes</h2>
          </div>
        </NavLink>
      </div>
    </header>
  );
}

export { Header };
