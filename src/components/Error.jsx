function Error({ icon, msg }) {
  return (
    <div className="w-full md:w-8/12 px-4 sm:px-0 mx-auto ">
      <p className="flex flex-col sm:flex-row text-center justify-center items-center gap-2 text-base sm:text-lg bg-red-200 py-6 rounded-lg mb-6 shadow-lg border border-red-500">
        {icon()} {msg}
      </p>
    </div>
  );
}

export { Error };
