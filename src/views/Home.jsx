import { Link } from "react-router-dom";
import { useState } from "react";
import { useStolenBikes } from "../hooks/useStolenBikes";
import initialState from "../initialState";

import { CardItem } from "../components/CardItem";
import { CardImage } from "../components/CardImage";
import { CardInfo } from "../components/CardInfo";
import { CardTitle } from "../components/CardTitle";
import { CardDescription } from "../components/CardDescription";
import { CardMoreInfo } from "../components/CardMoreInfo";
import { CardMoreInfoItem } from "../components/CardMoreInfoItem";
import { CardStatus } from "../components/CardStatus";
import { Pagination } from "../components/Pagination";
import { Loading } from "../components/Loading";
import { Search } from "../components/Search";
import { TotalResults } from "../components/TotalResults";
import { EmptyResults } from "../components/EmptyResults";
import { Error } from "../components/Error";

import {
  FaRegCalendarAlt,
  FaMapMarkerAlt,
  FaBarcode,
  FaTint,
  FaBicycle,
  FaSearch,
  FaExclamationCircle,
  FaExclamationTriangle,
} from "react-icons/fa";

function Home() {
  const [state] = useState(initialState);
  const [pageNumber, setPageNumber] = useState(initialState.pageNumber);
  const [search, setSearch] = useState(initialState.search);

  const API = process.env.REACT_APP_API;

  const stolenBikesUrl = `
    ${API}/search?page=${pageNumber}&per_page=${state.perPage}&query=${search}&stolenness=${state.stolenness}
  `;

  const counterStolenBikesUrl = `
    ${API}/search/count?&query=${search}&stolenness=stolen
  `;

  const { stolenBikes, counterStolenBikes, loading, error } = useStolenBikes(
    stolenBikesUrl,
    counterStolenBikesUrl
  );

  if (error) {
    return (
      <Error
        msg="!Oops, there was an error loading the data "
        icon={() => <FaExclamationTriangle />}
      />
    );
  }

  return (
    <>
      {loading && <Loading />}
      <div
        className={`transition-all duration-200 ${
          loading ? "opacity-0" : "opacity-1"
        } w-full md:w-8/12 px-4 md:px-0 md:mx-auto `}
      >
        <div className="flex flex-col sm:flex-row sm:justify-between items-center gap-3 mb-6">
          <Search icon={() => <FaSearch />} setSearch={setSearch} />
          <TotalResults total={counterStolenBikes} />
        </div>

        {!stolenBikes.length ? (
          <EmptyResults
            icon={() => <FaExclamationCircle />}
            message="Sorry, we did not find results in your search"
          />
        ) : (
          <>
            <section className={`mx-auto flex flex-col items-center`}>
              {stolenBikes.map(
                ({
                  id,
                  title,
                  description,
                  thumb,
                  date_stolen,
                  stolen_location,
                  frame_colors,
                  serial,
                  stolen,
                  status,
                }) => (
                  <Link to={`details/${id}`} key={id} className="block w-full">
                    <CardItem>
                      <CardImage
                        src={thumb}
                        alt={title}
                        defaultImg={() => <FaBicycle />}
                      />
                      <CardInfo>
                        <CardTitle text={title} />
                        <CardDescription text={description} />
                        <CardMoreInfo>
                          <CardMoreInfoItem
                            content={new Date(date_stolen).toDateString()}
                            icon={() => <FaRegCalendarAlt />}
                          />
                          <CardMoreInfoItem
                            content={stolen_location}
                            icon={() => <FaMapMarkerAlt />}
                          />
                          <CardMoreInfoItem
                            content={frame_colors.join(" - ")}
                            icon={() => <FaTint />}
                          />
                          <CardMoreInfoItem
                            content={serial}
                            icon={() => <FaBarcode />}
                          />
                        </CardMoreInfo>
                      </CardInfo>
                      <CardStatus stolen={stolen} status={status} />
                    </CardItem>
                  </Link>
                )
              )}
            </section>
            <Pagination
              pageCount={counterStolenBikes}
              pageSize={state.perPage}
              setPageNumber={setPageNumber}
            />
          </>
        )}
      </div>
    </>
  );
}

export { Home };
