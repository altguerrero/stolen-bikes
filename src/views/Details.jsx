import { useParams } from "react-router-dom";
import { useStolenBikeDetail } from "../hooks/useStolenBikeDetail";

import { Loading } from "../components/Loading";
import { Map } from "../components/Map";
import { DetailHeader } from "../components/DetailHeader";
import { DetailInfo } from "../components/DetailInfo";
import { DetailDescription } from "../components/DetailDescription";
import { DetailMoreInfo } from "../components/DetailMoreInfo";
import { DetailImages } from "../components/DetailImages";
import { EmptyResults } from "../components/EmptyResults";
import { Error } from "../components/Error";

import { FaExclamationCircle, FaExclamationTriangle } from "react-icons/fa";

function Details() {
  const { id } = useParams();

  const API = process.env.REACT_APP_API;
  const urlDetail = `${API}/bikes/${id}`;

  const { details, loading, error } = useStolenBikeDetail(urlDetail);

  if (error) {
    return (
      <Error
        msg="!Oops, there was an error loading the data "
        icon={() => <FaExclamationTriangle />}
      />
    );
  }

  return (
    <>
      {loading && <Loading />}
      {!details ? (
        <EmptyResults
          icon={() => <FaExclamationCircle />}
          message="Sorry, we did not find results in your search"
        />
      ) : (
        <div className="md:w-8/12 mx-auto px-4 md:px-0">
          <DetailHeader
            title={details.title}
            createdAt={
              details.stolen_record && details.stolen_record.created_at
            }
            stonlenLocation={details.stolen_location}
          />
          <Map coordinates={details.stolen_coordinates} />
          <DetailInfo>
            <DetailDescription
              title="Description of incident:"
              description={details.description}
            />
            <DetailMoreInfo
              title="More info:"
              items={[
                { title: "Serial", value: `${details.manufacturer_id}` },
                { title: "Model", value: `${details.frame_model}` },
                { title: "Frame size", value: `${details.frame_size}` },
                {
                  title: "Manufacturer",
                  value: `${details.manufacturer_name}`,
                },
                {
                  title: "Primary colors",
                  value: `(${details.frame_colors.join(",")})`,
                },
                {
                  title: "Frame Material",
                  value: `${details.frame_material_slug}`,
                },
              ]}
            />
            <DetailImages title="Images:" items={details.public_images} />
          </DetailInfo>
        </div>
      )}
    </>
  );
}

export { Details };
