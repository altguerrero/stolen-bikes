const initialState = {
  perPage: 10,
  search: "Berlin",
  stolenness: "stolen",
  pageNumber: 1,
};

export default initialState;
