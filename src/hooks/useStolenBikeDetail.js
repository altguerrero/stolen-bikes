import { useState, useEffect } from "react";
import axios from "axios";

const useStolenBikeDetail = (url) => {
  const [details, setDetails] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    axios
      .get(url)
      .then((res) => {
        setDetails(res.data.bike);
      })
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [url]);

  return {
    details,
    loading,
    error,
  };
};

export { useStolenBikeDetail };
