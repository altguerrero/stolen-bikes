import { useState, useEffect } from "react";
import axios from "axios";

const useStolenBikes = (stolenBikesUrl, counterStolenBikesUrl) => {
  const [stolenBikes, setStolenBikes] = useState([]);
  const [counterStolenBikes, setCounterStolenBikes] = useState(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    setLoading(true);
    const getStolenBikes = axios.get(stolenBikesUrl);
    const getStolenBikesCounter = axios.get(counterStolenBikesUrl);

    axios
      .all([getStolenBikes, getStolenBikesCounter])
      .then(
        axios.spread((...allData) => {
          setStolenBikes(allData[0].data.bikes);
          setCounterStolenBikes(allData[1].data.stolen);
        })
      )
      .catch((err) => {
        setError(err);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [counterStolenBikesUrl, stolenBikesUrl]);

  return {
    stolenBikes,
    counterStolenBikes,
    loading,
    error,
  };
};

export { useStolenBikes };
